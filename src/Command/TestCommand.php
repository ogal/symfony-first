<?php

namespace App\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class TestCommand extends Command
{
    protected static $defaultName = 'personal:test:hello';

    protected function configure()
    {
        $this
            ->setDescription('Add a short description for your command')
            ->addArgument('nombre', InputArgument::OPTIONAL, 'Nombre del usuario')
            ->addOption('ingles', 'x', InputOption::VALUE_NONE, 'Saludame en inglés')
            ->addOption('espanol', 'z', InputOption::VALUE_NONE, 'Saludame en castellano')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        $arg1 = $input->getArgument('nombre');

        if ($input->getOption('espanol')) {
            $io->note(print_r('Hola, como estás ' . $arg1));
        }else {
            $io->note(print_r('Hi, how are u? ' . $arg1));
        }

        $io->success('Comando creado satisfactoriamente! Pass --help to see your options.');

        return 0;
    }
}
